/*
@licstart  The following is the entire license notice for the
JavaScript code in this page.

Copyright (C) 2020 Moulinux

The JavaScript code in this page is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this page.
*/
const DICO_FR = {
    'enTete1': 'Calculette Eco-déplacements',
    'enTete2': `Calculez l'impact de vos déplacements quotidiens
        sur l\'environnement et vos dépenses`,
    'distance': 'Quelle est distance entre le domicile et le lieu de travail ?',
    'domicile1': 'J\'habite à ',
    'domicile2': ' km de mon travail.',
    'deplacement1': 'Choisir le ',
    'deplacement2': ' mode de déplacement',
    'modDeplacement': ['train', 'velo', 'voiture'],
    1: 'premier',
    2: 'second',
    'train': "le train",
    'velo': "le vélo",
    'voiture': "la voiture",
    'choix1': 'Je choisis ',
    'choix2': 'plutôt que ',
    'legDist': 'Distance Domicile-Travail',
    'legTrans': 'Modes de Transport',
};
const DICO_COUT = {
    'train': [14.62, 9.26],
    'velo': [0, 0],
    'voiture': [129.62, 50.65],
    'labTitre': ["Effet de serre", "Énergie"],
    'labUnite': [" kg eq. CO2", " l eq. pétrole"],
    'labEco1': ["J'évite ", "Je consomme "],
    'labEco2': [" kg eq. CO2 par an", " litres eq. pétrole en moins par an"],
    'labDep1': ["J'émets ", "Je consomme "],
    'labDep2': [" kg eq. CO2 en plus par an", " litres eq. pétrole en plus par an"]
};

/**
* Calcule les coûts de déplacement.
* 
* @param lesCouts liste contenant les éléments kilométriques liés à un mode de déplacement
* @param km distance entre le domicile et le lieu de travail
* @return liste contenant les coûts kilométriques liés à un mode de déplacement
*/
function calculeCoutKm(lesCouts, km) {
    let res = [];   // Initialiser la liste des résultats
    for (indice in lesCouts) {
        res[indice] = Math.round(lesCouts[indice]*km*100)/100;
    }
    return res;
}

/**
* Compare les coûts de déplacement.
* 
* @param lesCouts1 liste contenant les coûts calculés pour le premier mode de déplacement
* @param lesCouts2 liste contenant les coûts calculés pour le second mode de déplacement
* @return liste contenant les coûts kilométriques comparés
*/
function compareCoutKm(lesCouts1, lesCouts2) {
    let diff = [];
    for (indice in lesCouts1) {
        diff[indice] = Math.round((lesCouts2[indice]-lesCouts1[indice])*100)/100;
    }
    return diff;
}

/**
* Affiche les coûts de déplacement.
* 
* @return rien
*/
function afficheCout() {
    let km = document.getElementById("distance").value;
    let modeDep1 = document.getElementById("dep1").value;
    let modeDep2 = document.getElementById("dep2").value;
    let mesCoutsDep1 = calculeCoutKm(DICO_COUT[modeDep1], km);
    let mesCoutsDep2 = calculeCoutKm(DICO_COUT[modeDep2], km);
    
    document.getElementById("resume").innerHTML = DICO_FR['domicile1'] + km + DICO_FR['domicile2'];
    
    document.getElementById("choix1").innerHTML = DICO_FR['choix1'] + DICO_FR[modeDep1];
    document.getElementById("choix2").innerHTML = DICO_FR['choix2'] + DICO_FR[modeDep2];
    
    document.getElementById("serre1").innerHTML = mesCoutsDep1[0] + DICO_COUT['labUnite'][0];
    document.getElementById("engie1").innerHTML = mesCoutsDep1[1] + DICO_COUT['labUnite'][1];

    document.getElementById("serre2").innerHTML = mesCoutsDep2[0] + DICO_COUT['labUnite'][0];
    document.getElementById("engie2").innerHTML = mesCoutsDep2[1] + DICO_COUT['labUnite'][1];
        
    var diffCouts = compareCoutKm(mesCoutsDep1, mesCoutsDep2);
    
    if (diffCouts[0] < 0) {
        document.getElementById("diffSerre").innerHTML = DICO_COUT['labDep1'][0] + Math.abs(diffCouts[0]) + DICO_COUT['labDep2'][0];
        document.getElementById("diffEngie").innerHTML = DICO_COUT['labDep1'][1] + Math.abs(diffCouts[1]) + DICO_COUT['labDep2'][1];
    } else {
        document.getElementById("diffSerre").innerHTML = DICO_COUT['labEco1'][0] + Math.abs(diffCouts[0]) + DICO_COUT['labEco2'][0];
        document.getElementById("diffEngie").innerHTML = DICO_COUT['labEco1'][1] + Math.abs(diffCouts[1]) + DICO_COUT['labEco2'][1];
    }
}
